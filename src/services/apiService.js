import axios from "axios";


function getApiURL(model, id) {
    const api_token = 'keyEC9F0usYdVoWzg'
    switch(model) {
        case "lists":
            return "https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/tblWcsEyTOIf47ZuQ?api_key=" + api_token;
        case "details":
            return `https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/People/${id}?api_key=` + api_token;
        case "friendRequest":
            return "https://api.airtable.com/v0/app7cLoZ4xMOrQvKR/Friend%20request";
        default:
            return "";
    }
}

    async function apiGetLists(){
    try {
        let lists = await axios.get(getApiURL('lists'), )
        return lists.data
    } catch (error) {
        console.log(error);
    }
}
    async function apiGetDetails(id){
        try {
            let lists = await axios.get(getApiURL('details', id), )
            return lists.data
        } catch (error) {
            console.log(error);
        }
}

export {apiGetLists, apiGetDetails}