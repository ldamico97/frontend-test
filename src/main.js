import { createApp } from 'vue'
import { createRouter,createWebHistory }  from 'vue-router'
import App from './App.vue'
import './assets/tailwind.css'
import Index from './views/Index'
import PersonDetails from './views/PersonDetails'
const routes = [
    {
        path: '/', component: Index,
    },
    {
        path: '/personDetails/:id', component: PersonDetails,
    }
]

const router = createRouter({
    routes, 
    history: createWebHistory(),
  })
const app = createApp(App)
app.use(router);
app.mount('#app');
